package com.imgtec.radiorec.server.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Recording {

  @Id
  @GeneratedValue
  private Long id;

  @Column(nullable = false)
  private String userId;

  @Column(nullable = false)
  private long scheduledRecordingId;

  /**
   * Flow.LiveRadioItem.ContentItemId
   */
  @Column(nullable = false)
  private String flowRadioId;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private Date startTime;

  @Column(nullable = false)
  private int durationSecs;

  @Column(nullable = false)
  private String url;

  @Column(nullable = true)
  private Integer bitRate;



  public Recording() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getFlowRadioId() {
    return flowRadioId;
  }

  public void setFlowRadioId(String flowRadioId) {
    this.flowRadioId = flowRadioId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public int getDurationSecs() {
    return durationSecs;
  }

  public void setDurationSecs(int durationSecs) {
    this.durationSecs = durationSecs;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getBitRate() {
    return bitRate;
  }

  public void setBitRate(Integer bitRate) {
    this.bitRate = bitRate;
  }

  public long getScheduledRecordingId() {
    return scheduledRecordingId;
  }

  public void setScheduledRecordingId(long scheduledRecordingId) {
    this.scheduledRecordingId = scheduledRecordingId;
  }
}
