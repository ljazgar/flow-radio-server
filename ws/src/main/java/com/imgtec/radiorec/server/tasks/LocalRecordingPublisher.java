package com.imgtec.radiorec.server.tasks;

import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class LocalRecordingPublisher implements RecordingPublisher {

  private static final Logger logger = LoggerFactory.getLogger(LocalRecordingPublisher.class);

  private final File rootDirectory;
  private final String urlPrefix;
  private final PublishedRecordingPathEvaluator pathEvaluator;

  public LocalRecordingPublisher(String rootDirectory, String urlPrefix,
                                 PublishedRecordingPathEvaluator pathEvaluator) {
    this.pathEvaluator = pathEvaluator;
    this.rootDirectory = new File(rootDirectory);
    this.urlPrefix = urlPrefix != null ? urlPrefix : "";
    logger.debug("Creating LocalRecordingPublisher. rootDir: {}  urlPrefix: {}", rootDirectory, urlPrefix);
  }

  @Override
  public String publish(File tmpRecordingFile, RecordingTaskMetaInfo metaInfo) {

    String relativeFilePath = pathEvaluator.evaluateRelativePath(metaInfo);

    String targetFilePath = String.format("%s/%s", rootDirectory.getAbsolutePath(), relativeFilePath);
    File targetFile = new File(targetFilePath);

    try {
      Files.createParentDirs(targetFile);
      Files.move(tmpRecordingFile, targetFile);
    } catch (IOException e) {
      throw new PublishingException(
          String.format("Cannot move file %s to target place: %s", tmpRecordingFile, targetFile), e);
    }

    return String.format("%s/%s", urlPrefix, relativeFilePath);
  }

}
