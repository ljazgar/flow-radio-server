package com.imgtec.radiorec.server.shoutcast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShoutcastStreamUrlResolver {

  private static final Logger logger = LoggerFactory.getLogger(ShoutcastStreamUrlResolver.class);

  private final URLToStringDownloader urlToStringDownloader;
  private final M3UParser m3UParser;
  private final PLSParser plsParser;

  @Autowired
  public ShoutcastStreamUrlResolver(URLToStringDownloader urlToStringDownloader, M3UParser m3UParser, PLSParser plsParser) {
    this.urlToStringDownloader = urlToStringDownloader;
    this.m3UParser = m3UParser;
    this.plsParser = plsParser;
  }

  public String resolveStreamUrl(String flowRadioUrl) {

    String m3u = urlToStringDownloader.download(flowRadioUrl);
    if (m3u == null) {
      return null;
    }

    String urlFromM3U = m3UParser.parseM3U(m3u);
    logger.debug("Url extracted from m3u: {}", urlFromM3U);
    if (urlFromM3U == null) {
      logger.error("Extraction of url from M3U playlist is failed");
      return null;
    }

    String targetUrl;
    if (urlFromM3U.endsWith(".pls")) {
      logger.debug("Url directs to PLS file. Download and extract url from it");
      String pls = urlToStringDownloader.download(urlFromM3U);
      targetUrl = plsParser.parsePLS(pls);
      if (targetUrl == null) {
        logger.error("Extraction of url from PLS is failed");
        return null;
      }
    } else {
      targetUrl = urlFromM3U;
    }

    return targetUrl;
  }
}
