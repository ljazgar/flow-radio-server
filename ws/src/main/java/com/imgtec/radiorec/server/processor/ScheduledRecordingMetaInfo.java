package com.imgtec.radiorec.server.processor;

import com.imgtec.radiorec.server.model.ScheduledRecording;
import com.imgtec.radiorec.server.tasks.RecordingTaskMetaInfo;

public class ScheduledRecordingMetaInfo implements RecordingTaskMetaInfo {

  private final ScheduledRecording scheduledRecording;

  public ScheduledRecordingMetaInfo(ScheduledRecording scheduledRecording) {
    this.scheduledRecording = scheduledRecording;
  }


  @Override
  public String getUserId() {
    return scheduledRecording.getUserId();
  }

  @Override
  public String getFlowRadioId() {
    return scheduledRecording.getFlowRadioId();
  }

  @Override
  public String getRecordingName() {
    return scheduledRecording.getName();
  }
}
