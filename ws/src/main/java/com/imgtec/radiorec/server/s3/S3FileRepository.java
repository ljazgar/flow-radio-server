package com.imgtec.radiorec.server.s3;

import com.google.common.io.Closeables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StreamUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class S3FileRepository {

  private static final Logger logger = LoggerFactory.getLogger(S3FileRepository.class);

  private final String amazonServerAddress;
  private final String amazonKeyId;
  private final String amazonKey;
  private final String bucket;

  private final Mac mac;

  public S3FileRepository(
      String amazonServerAddress,
      String amazonKeyId,
      String amazonKey,
      String bucket) {
    this.amazonServerAddress = amazonServerAddress;
    this.amazonKeyId = amazonKeyId;
    this.amazonKey = amazonKey;
    this.bucket = bucket;
    this.mac = createMac(amazonKey);
    logger.debug("Creating S3FileRepository: server: {}  amazonKeyId: {}  bucket: {}",
        amazonServerAddress, amazonKeyId, bucket);
  }


  private static Mac createMac(String awsSecretKey) {
    try {
      byte[] keyBytes = awsSecretKey.getBytes("UTF8");
      javax.crypto.spec.SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
      Mac mac = Mac.getInstance("HmacSHA1");
      mac.init(signingKey);
      return mac;
    } catch (Exception e){
      throw new IllegalArgumentException("Cannot create Mac from awsSecretKey!", e);
    }
  }

  public void putFile(String targetPath, File file) throws S3Exception {
    InputStream in = null;
    try {
      in = new FileInputStream(file);
    } catch (FileNotFoundException e) {
      throw new S3Exception(String.format("Cannot open file to upload to S3: %s", file.getAbsolutePath()), e);
    }

    try {
      putFile(targetPath, in);
    } finally {
      Closeables.closeQuietly(in);
    }
  }

  private void putFile(String path, InputStream blobInputStream) throws S3Exception {

    HttpURLConnection httpConn = prepareConnection(path);
    OutputStream putRequestBodyStream = null;
    try {
      putRequestBodyStream = httpConn.getOutputStream();
      StreamUtils.copy(blobInputStream, putRequestBodyStream);
      putRequestBodyStream.close();
    } catch (IOException e) {
      throw new S3Exception("Exception while sending body of PUT request", e);
    } finally {
      Closeables.closeQuietly(blobInputStream);
    }

    int statusCode;
    try {
      statusCode = httpConn.getResponseCode();
    } catch (IOException e) {
      logger.debug("Cannot resolve HTTP errorCode");
      throw new S3Exception(e);
    }

    if ((statusCode/100) != 2){
      String errorString = loadHttpErrorMessage(httpConn);
      throw new S3Exception(String.format("File upload to S3 failed: %d   Message: %s", statusCode, errorString));
    }
  }

  private static String loadHttpErrorMessage(HttpURLConnection httpConn) {

    InputStream in = null;
    try {
      in = httpConn.getErrorStream();
      if (in == null) {
        in = (InputStream) httpConn.getContent();
      }

      return StreamUtils.copyToString(in, Charset.forName("UTF-8"));
    } catch (IOException e) {
      logger.debug("Cannot get error message from http response");
      return null;
    } finally {
      Closeables.closeQuietly(in);
    }
  }

  /**
   * Prepares HTTP connection ( creates HTTP header and signs it)
   * @param path path to file in S3 Bucket
   * @return HttpURLConnection prepared connection
   */
  private HttpURLConnection prepareConnection(String path) throws S3Exception {
    // Data required to create signature
    String method = "PUT";
    String contentMD5 = "";
    String contentType = "";
    String metadataHeaders = "x-amz-acl:public-read";
    String date = getTimeStamp();
    String fullPath = bucket + "/" + path;

    // Signature generation
    StringBuffer buf = new StringBuffer();
    buf.append(method).append("\n");
    buf.append(contentMD5).append("\n");
    buf.append(contentType).append("\n");
    buf.append(date).append("\n");
    buf.append(metadataHeaders).append("\n");
    buf.append(fullPath);

    String signature = sign(mac, buf.toString());

    //Preparing HTTP header
    HttpURLConnection httpConn;
    try {
      URL url = new URL("http", amazonServerAddress, 80, fullPath);

      httpConn = (HttpURLConnection) url.openConnection();
      httpConn.setDoInput(true);
      httpConn.setDoOutput(true);
      httpConn.setUseCaches(false);
      httpConn.setDefaultUseCaches(false);
      httpConn.setAllowUserInteraction(true);
      httpConn.setRequestMethod(method);
      httpConn.setRequestProperty("Date", date);
      httpConn.setRequestProperty("Content-Length", "0");
      httpConn.setRequestProperty("x-amz-acl", "public-read");
      String AWSAuth = "AWS " + this.amazonKeyId + ":" + signature;
      httpConn.setRequestProperty("Authorization", AWSAuth);
    } catch (MalformedURLException e) {
      logger.debug("Malformed amazon repository url.");
      throw new S3Exception("Malformed amazon repository url.", e);
    } catch(ProtocolException e) {
      logger.debug("Wrong protocol settings. File Upload stopped.");
      throw new S3Exception(e.getMessage());
    } catch (IOException e) {
      logger.debug("Cannot open connection with Amazon S3 file upload stopped");
      throw new S3Exception("Cannot open connection with Amazon S3", e);
    }

    return httpConn;
  }

  /**
   * Creates current date in format required by Amazon
   * @return String Current date in specific format
   */
  private static String getTimeStamp() {
    String fmt = "EEE, dd MMM yyyy HH:mm:ss ";
    SimpleDateFormat df = new SimpleDateFormat(fmt, Locale.US);
    df.setTimeZone(TimeZone.getTimeZone("GMT"));
    return df.format(new Date()) + "GMT";
  }

  /**
   * Signatures data.
   * @param mac Mac initialized with signing key
   * @param data String which is to sign
   * @return String Signature in Base64 format
   */
  private static String sign(Mac mac, String data)
  {
    // Signed String must be BASE64 encoded.
    byte[] bytes;
    try {
      bytes = data.getBytes("UTF8");
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException("It's impossible, but UTF8 encoding does not exist on this JVM", e);
    }
    byte[] signBytes = mac.doFinal(bytes);
    byte[] signatureBytes = Base64.getEncoder().encode(signBytes);
    String signature = new String(signatureBytes);
    return signature;
  }

}
