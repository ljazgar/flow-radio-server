package com.imgtec.radiorec.server.tasks;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.imgtec.radiorec.server.shoutcast.RadioRecorder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

@Component
public class LocalThreadsRecordingTasksManager implements RecordingTasksManager {

  private static final Logger logger = LoggerFactory.getLogger(LocalThreadsRecordingTasksManager.class);

  private final ListeningExecutorService executorService =
      MoreExecutors.listeningDecorator(Executors.newCachedThreadPool(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
          Thread newThread = Executors.defaultThreadFactory().newThread(r);
          newThread.setDaemon(true);
          return newThread;
        }
      }));

  private final RadioRecorder radioRecorder;
  private final RecordingPublisher recordingPublisher;

  @Autowired
  public LocalThreadsRecordingTasksManager(RadioRecorder radioRecorder,
                                           @Qualifier("recordingPublisher") RecordingPublisher recordingPublisher) {
    this.radioRecorder = radioRecorder;
    this.recordingPublisher = recordingPublisher;
  }

  @Override
  public ListenableFuture<String> startRecording(String flowRadioUrl, int durationSecs, RecordingTaskMetaInfo metaInfo) {
    Callable<String> task = new RecordingTask(flowRadioUrl, durationSecs, metaInfo);
    return executorService.<String>submit(task);
  }

  private class RecordingTask implements Callable<String> {
    private final String flowRadioUrl;
    private final int durationSecs;
    private final RecordingTaskMetaInfo metaInfo;

    public RecordingTask(String flowRadioUrl, int durationSecs, RecordingTaskMetaInfo metaInfo) {
      this.flowRadioUrl = flowRadioUrl;
      this.durationSecs = durationSecs;
      this.metaInfo = metaInfo;
    }

    @Override
    public String call() throws Exception {
      File tmpRecordingFile = radioRecorder.recordRadio(flowRadioUrl, durationSecs);
      String url = recordingPublisher.publish(tmpRecordingFile, metaInfo);
      tmpRecordingFile.delete();
      return url;
    }
  }

}
