package com.imgtec.radiorec.server.shoutcast;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class PLSParser {

  public static final String FILE1 = "File1=";

  public static class PLSParsingException extends RuntimeException {
    public PLSParsingException(String message) {
      super(message);
    }
  }

  /**
   *
   * @param pls
   * @return URL
   */
  String parsePLS(String pls) {
    if (pls == null) {
      throw new IllegalArgumentException("PLS agument is null");
    }
    try (Scanner scanner = new Scanner(pls)) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine().trim();
        if (line.startsWith(FILE1)) {
          return line.substring(FILE1.length());
        }
      }
    }
    throw new PLSParsingException("Could not find any URL in PLS file");
  }

}
