package com.imgtec.radiorec.server.ws;

import com.imgtec.radiorec.server.model.Recording;
import com.imgtec.radiorec.server.model.RecordingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/user/{userId}/recordings")
public class RecordingRestController {

  private final RecordingRepository recordingRepository;

  @Autowired
  public RecordingRestController(RecordingRepository recordingRepository) {
    this.recordingRepository = recordingRepository;
  }

  @RequestMapping(value = "/{recordingId}", method = RequestMethod.GET)
  Recording readRecording(@PathVariable String userId, @PathVariable Long recordingId) {
    Recording recording = this.recordingRepository.findOne(recordingId);
    if (recording != null && recording.getUserId().equals(userId)) {
      return recording;
    } else {
      throw new RecordingNotFoundException(recordingId);
    }
  }

  @RequestMapping(method = RequestMethod.GET)
  Collection<Recording> readRecordings(@PathVariable String userId) {
    return this.recordingRepository.findByUserIdOrderByStartTimeDesc(userId);
  }

}

@ResponseStatus(HttpStatus.NOT_FOUND)
class RecordingNotFoundException extends RuntimeException {
  public RecordingNotFoundException(Long recordingId) {
    super("Could not find recording with id '" + recordingId + "'");
  }
}