package com.imgtec.radiorec.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Device {

  @Id
  @GeneratedValue
  private Long id;

  @Column(nullable = false)
  private String userId;

  @Column(nullable = false)
  private String gcmToken;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getGcmToken() {
    return gcmToken;
  }

  public void setGcmToken(String gcmToken) {
    this.gcmToken = gcmToken;
  }

}
