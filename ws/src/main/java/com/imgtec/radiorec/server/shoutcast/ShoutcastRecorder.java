package com.imgtec.radiorec.server.shoutcast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URI;

@Component
public class ShoutcastRecorder {

  private static final Logger logger = LoggerFactory.getLogger(ShoutcastRecorder.class);

  public static class ShoutcastRecordingException extends RuntimeException {
    public ShoutcastRecordingException(String message) {
      super(message);
    }
    public ShoutcastRecordingException(String message, Throwable cause) {
      super(message, cause);
    }
  }

  private final ClientHttpRequestFactory clientHttpRequestFactory;

  @Autowired
  public ShoutcastRecorder(ClientHttpRequestFactory clientHttpRequestFactory) {
    this.clientHttpRequestFactory = clientHttpRequestFactory;
  }

  public File recordStream(String shoutcastStreamUrl, int durationSecs) {
    File tmpFile;
    try {
      tmpFile = File.createTempFile("shoutcast-rec-", ".mp3");
    } catch (IOException e) {
      throw new ShoutcastRecordingException("Cannot create temp file ", e);
    }

    logger.debug("Stream will be recorded to temp file: {}", tmpFile.getAbsolutePath());
    ClientHttpRequest request = null;
    try {
      request = clientHttpRequestFactory.createRequest(URI.create(shoutcastStreamUrl), HttpMethod.GET);
    } catch (IOException e) {
      throw new ShoutcastRecordingException(
          String.format("Cannot create http request to url: %s", shoutcastStreamUrl), e);
    }

    try ( ClientHttpResponse response = request.execute();
          OutputStream fileOutputStream = new FileOutputStream(tmpFile);
        ) {
      logger.debug("Request executed");
      InputStream bodyInputStream = response.getBody();

      long startTime = System.currentTimeMillis();
      int correctedDurationSecs = (durationSecs > 3) ? durationSecs - 3 : durationSecs;
      long endTime = startTime + 1000*correctedDurationSecs;

      logger.debug("Recording body");

      byte[] buf = new byte[1024];
      int len;
      while ( System.currentTimeMillis() < endTime
             && (len=bodyInputStream.read(buf))>0 ) {
        fileOutputStream.write(buf,0,len);
      }

      String reason = System.currentTimeMillis() >= endTime ? "time lapse" : "end of stream";
      logger.debug("Recording finished due to {}.", reason);
    } catch (IOException e) {
      throw new ShoutcastRecordingException(
          String.format("Exception while downloading stream from url: %s", shoutcastStreamUrl), e);
    }

    return tmpFile;
  }
}
