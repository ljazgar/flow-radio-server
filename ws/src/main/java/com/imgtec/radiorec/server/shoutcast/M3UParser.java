package com.imgtec.radiorec.server.shoutcast;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class M3UParser {

  public static class M3UParsingException extends RuntimeException {
    public M3UParsingException(String message) {
      super(message);
    }
  }

  /**
   *
   * @param m3u
   * @return URL
   */
  String parseM3U(String m3u) {
    if (m3u == null) {
      throw new IllegalArgumentException("m3u parameter is null");
    }
    try (Scanner scanner = new Scanner(m3u)) {
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine().trim();
        if (line.startsWith("http:")) {
          return line;
        }
      }
    }
    throw new M3UParsingException("Could not find any url in M3U playlist");
  }

}
