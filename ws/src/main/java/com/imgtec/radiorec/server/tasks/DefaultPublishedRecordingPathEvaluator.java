package com.imgtec.radiorec.server.tasks;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class DefaultPublishedRecordingPathEvaluator implements PublishedRecordingPathEvaluator {

  private final static Random random = new Random();

  @Override
  public String evaluateRelativePath(RecordingTaskMetaInfo metaInfo) {
    String subDir = (metaInfo != null && metaInfo.getUserId() != null) ? metaInfo.getUserId() : "unknown";
    String subSubDir = (metaInfo != null && metaInfo.getFlowRadioId() != null) ? metaInfo.getFlowRadioId() : "unknown";
    String fileName = evaluateFileName(metaInfo);

    return String.format("%s/%s/%s", subDir, subSubDir, fileName);
  }

  private String evaluateFileName(RecordingTaskMetaInfo metaInfo) {
    String rndString = String.valueOf(random.nextInt(10000));

    DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    String datePart = dateFormat.format(new Date());

    return String.format("%s-%s.mp3", datePart, rndString);
  }

}
