package com.imgtec.radiorec.server.tasks;

public interface PublishedRecordingPathEvaluator {

  String evaluateRelativePath(RecordingTaskMetaInfo metaInfo);

}
