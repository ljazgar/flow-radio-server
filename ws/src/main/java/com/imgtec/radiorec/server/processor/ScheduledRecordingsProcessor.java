package com.imgtec.radiorec.server.processor;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.imgtec.radiorec.server.gcm.GcmSender;
import com.imgtec.radiorec.server.model.*;
import com.imgtec.radiorec.server.tasks.RecordingTaskMetaInfo;
import com.imgtec.radiorec.server.tasks.RecordingTasksManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class ScheduledRecordingsProcessor {

  private static final Logger logger = LoggerFactory.getLogger(ScheduledRecordingsProcessor.class);
  private static final int MAX_WAIT_TIME_MS = 5 * 60 * 1000;

  private final ScheduledRecordingRepository scheduledRecordingRepository;
  private final RecordingRepository recordingRepository;
  private final DeviceRepository deviceRepository;
  private final RecordingTasksManager recordingTasksManager;
  private final GcmSender gcmSender;
  private final ProcessingThread processingThread = new ProcessingThread();

  @Autowired
  public ScheduledRecordingsProcessor(ScheduledRecordingRepository scheduledRecordingRepository,
                                      RecordingRepository recordingRepository,
                                      DeviceRepository deviceRepository,
                                      RecordingTasksManager recordingTasksManager,
                                      GcmSender gcmSender) {
    this.scheduledRecordingRepository = scheduledRecordingRepository;
    this.recordingRepository = recordingRepository;
    this.deviceRepository = deviceRepository;
    this.recordingTasksManager = recordingTasksManager;
    this.gcmSender = gcmSender;
  }


  public void start() {
    processingThread.start();
  }

  public void scheduleChanged() {
    processingThread.signal();
  }

  private class ProcessingThread extends Thread {

    private final ReentrantLock lock = new ReentrantLock();
    private final Condition signalCondition = lock.newCondition();
    private final AtomicBoolean signalFlag = new AtomicBoolean(false);

    @Override
    public void run() {
      logger.info("Scheduled recording processor: STARTED");

      try {
        while (true) {
          ScheduledRecording next =
              scheduledRecordingRepository.findTopByStateOrderByStartTimeAsc(ScheduledRecording.State.WAITING);
          if (next != null) {
            logger.debug("Next recording: {}  UserId: {} Name: {}", next.getId(), next.getUserId(), next.getName());
          } else {
            logger.debug("No next recordings");
          }

          if (next == null) {
            sleepUntilSignal(MAX_WAIT_TIME_MS);
            continue;
          }

          long now = System.currentTimeMillis();
          long nextStartTime = next.getStartTime().getTime();
          long timeLeft = nextStartTime - now;
          logger.debug("Time left to next recording {}", timeLeft);

          if (now < nextStartTime) {
            boolean notified = sleepUntilSignal(Math.min(timeLeft, MAX_WAIT_TIME_MS));
            now = System.currentTimeMillis();
            timeLeft = nextStartTime - now;
            if (notified) {
              logger.debug("Signal came that sth changed in schedule. Time left: {}. Reload", timeLeft);
            } else { // timeout
              if (now >= nextStartTime) {
                logger.debug("After waiting. Time left: {}. Starting recording task", timeLeft);
                startTask(next);
              } else {
                logger.warn("Not enough waiting: now:{}  startTime:{}   Time left: {}. Reload.",
                    now, nextStartTime, timeLeft);
              }
              continue;
            }

          } else if (nextStartTime <= now && now <= nextStartTime+10000) {
            logger.debug("Time left: {}. Starting recording task", timeLeft);
            startTask(next);
          } else {
            logger.debug("Time left: {}. Set state OMITTED", timeLeft);
            changeState(next.getId(), ScheduledRecording.State.OMITTED);
          }
        }
      } catch (InterruptedException e) {
        logger.info("Scheduled recording processor: Interrupted -> Going to stop");
      }
      logger.info("Scheduled recording processor: STOP");
    }

    private void sleepUntilSignal() throws InterruptedException {
      sleepUntilSignal(Long.MAX_VALUE);
    }

    private boolean sleepUntilSignal(long timeoutMs) throws InterruptedException {
      logger.debug("Going sleep for: {}", timeoutMs);
      lock.lock();
      try {
        boolean notified = true;
        while (!signalFlag.get() && notified)  {
          notified = signalCondition.await(timeoutMs, TimeUnit.MILLISECONDS);
        }
        logger.debug( notified ? "awaken" : "timeout");
        return notified;
      } finally {
        signalFlag.set(false);
        lock.unlock();
      }
    }

    public void signal() {
      lock.lock();
      try {
        signalFlag.set(true);
        signalCondition.signal();
      } finally {
        lock.unlock();
      }
    }
  }

  private void startTask(ScheduledRecording scheduledRecording) {
    final long scheduledRecordingId = scheduledRecording.getId();
    boolean changed = changeState(scheduledRecordingId, ScheduledRecording.State.RECORDING);
    if (!changed) {
      logger.warn("Cannot start task on scheduled recording id={} because it doesn't exist", scheduledRecordingId);
      return;
    }
    long endTime = scheduledRecording.getEndTime().getTime();
    long startTime = scheduledRecording.getStartTime().getTime();
    int durationSec = (int)(endTime - startTime) / 1000;
    long realStartTime = System.currentTimeMillis();

    RecordingTaskMetaInfo metaInfo = new ScheduledRecordingMetaInfo(scheduledRecording);
    ListenableFuture<String> urlFuture =
        recordingTasksManager.startRecording(scheduledRecording.getRadioStreamUrl(), durationSec, metaInfo );
    logger.debug("Task for recording scheduled recording id={} STARTED", scheduledRecording.getId());

    Futures.addCallback(urlFuture, new FutureCallback<String>() {
      @Override
      public void onSuccess(String recordingUrl) {
        logger.debug("Task for recording scheduled recording id={} finished with SUCCESS. Url: {}",
            scheduledRecording.getId(), recordingUrl);
        if (changeState(scheduledRecordingId, ScheduledRecording.State.DONE)) {
          createRecording(scheduledRecording, recordingUrl, realStartTime, durationSec);
          createNextScheduledRecording(scheduledRecordingId);
        } else {
          logger.warn("Cannot change state, create recording from scheduled recording id={} because it doesn't exist", scheduledRecordingId);
        }
      }

      @Override
      public void onFailure(Throwable t) {
        logger.error("Task for recording scheduled recording id={} finished with ERROR.", t);
        if (changeState(scheduledRecordingId, ScheduledRecording.State.ERROR)) {
          createNextScheduledRecording(scheduledRecordingId);
        } else {
          logger.warn("Cannot change state of scheduled recording id={} because it doesn't exist", scheduledRecordingId);
        }
      }
    });
  }

  private boolean changeState(long scheduledRecordingId, ScheduledRecording.State newState) {
    ScheduledRecording scheduledRecording = scheduledRecordingRepository.findOne(scheduledRecordingId);
    if (scheduledRecording == null) {
      logger.error("Cannot change state of scheduled recording: {}. It doesn't exist", scheduledRecordingId);
      return false;
    }
    scheduledRecording.setState(newState);
    scheduledRecordingRepository.saveAndFlush(scheduledRecording);
    logger.debug("Scheduled recording: {} -> state changed to {}", scheduledRecordingId, newState);
    return true;
  }

  private void createNextScheduledRecording(long oldScheduledRecordingId) {
    ScheduledRecording oldScheduledRecording = scheduledRecordingRepository.findOne(oldScheduledRecordingId);
    if (oldScheduledRecording == null) {
      logger.error("Cannot create next scheduled recording for scheduled recording: {}. It doesn't exist", oldScheduledRecordingId);
      return;
    }
    ScheduledRecording.RepeatOption oldRepeatOption = oldScheduledRecording.getRepeatOption();
    if (oldRepeatOption == null) {
      return;
    }
    logger.debug("ScheduledRecording: {} - Repeat option: {} -> Creating next scheduled recording",
        oldScheduledRecordingId, oldRepeatOption);

    ScheduledRecording newScheduledRecording = new ScheduledRecording();
    newScheduledRecording.setId(null);
    newScheduledRecording.setUserId(oldScheduledRecording.getUserId());
    newScheduledRecording.setState(ScheduledRecording.State.WAITING);
    newScheduledRecording.setFlowRadioId(oldScheduledRecording.getFlowRadioId());
    newScheduledRecording.setName(oldScheduledRecording.getName());

    long oldStartTime = oldScheduledRecording.getStartTime().getTime();
    long oldDuration = oldScheduledRecording.getEndTime().getTime() - oldStartTime;

    Calendar newStartTimeCalendar = Calendar.getInstance();
    newStartTimeCalendar.setTimeInMillis(oldStartTime);
    newStartTimeCalendar.add(getCalendarFieldForRepeatOption(oldRepeatOption), 1);
    long newStartTime = newStartTimeCalendar.getTimeInMillis();
    Date newStartTimeDate = new Date(newStartTime);
    long newEndTime = newStartTime + oldDuration;
    Date newEndTimeDate = new Date(newEndTime);
    logger.debug("Time of next scheduled recording is: {} - {}", newStartTimeDate, newEndTimeDate);

    newScheduledRecording.setStartTime(newStartTimeDate);
    newScheduledRecording.setEndTime(newEndTimeDate);

    newScheduledRecording.setRepeatOption(oldRepeatOption);
    newScheduledRecording.setRadioStreamUrl(oldScheduledRecording.getRadioStreamUrl());

    scheduledRecordingRepository.saveAndFlush(newScheduledRecording);
    logger.debug("Next Scheduled recording saved.");
  }

  private static int getCalendarFieldForRepeatOption(ScheduledRecording.RepeatOption repeatOption) {
    switch (repeatOption) {
      case WEEKLY: return Calendar.WEEK_OF_YEAR;
      case MONTHLY: return Calendar.MONTH;
      case DAILY:
      default: return Calendar.DAY_OF_MONTH;
    }
  }

  private void createRecording(ScheduledRecording scheduledRecording,
                               String recordingUrl, long startTime, int durationSecs) {
    Recording newRecording = new Recording();
    newRecording.setUserId(scheduledRecording.getUserId());
    newRecording.setScheduledRecordingId(scheduledRecording.getId());
    newRecording.setFlowRadioId(scheduledRecording.getFlowRadioId());
    newRecording.setName(scheduledRecording.getName());
    newRecording.setStartTime(new Date(startTime));
    newRecording.setDurationSecs(durationSecs);
    newRecording.setUrl(recordingUrl);
    Recording savedRecording = recordingRepository.saveAndFlush(newRecording);
    logger.debug("Recording saved: Id: {}", savedRecording.getId());

    sendNewRecordingNotifications(newRecording.getUserId(), newRecording);
  }

  private void sendNewRecordingNotifications(String userId, Recording newRecording) {
    Collection<Device> devices = deviceRepository.findByUserId(userId);
    for (Device device : devices) {
      gcmSender.sendNewRecordingNotification(device.getGcmToken(), newRecording);
    }
  }
}
