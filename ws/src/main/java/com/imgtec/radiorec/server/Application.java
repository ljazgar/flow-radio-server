package com.imgtec.radiorec.server;

import com.imgtec.radiorec.server.gcm.GcmSender;
import com.imgtec.radiorec.server.processor.ScheduledRecordingsProcessor;
import com.imgtec.radiorec.server.s3.S3FileRepository;
import com.imgtec.radiorec.server.s3.S3RecordingPublisher;
import com.imgtec.radiorec.server.tasks.DefaultPublishedRecordingPathEvaluator;
import com.imgtec.radiorec.server.tasks.LocalRecordingPublisher;
import com.imgtec.radiorec.server.tasks.PublishedRecordingPathEvaluator;
import com.imgtec.radiorec.server.tasks.RecordingPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {

  private static final Logger logger = LoggerFactory.getLogger(Application.class);

  @Autowired
  ApplicationContext applicationContext;

  @Autowired
  Environment env;

  @Bean
  ClientHttpRequestFactory clientHttpRequestFactory() {
    return new SimpleClientHttpRequestFactory();
  }


  @Bean @Lazy
  RecordingPublisher recordingPublisher() {
    String publisher = env.getRequiredProperty("publisher");
    logger.debug("Recording publisher factory method: property publisher: {}", publisher);

    if (publisher.equalsIgnoreCase("local")) {
      return localRecordingPublisher();
    } else if (publisher.equalsIgnoreCase("s3")) {
      return s3RecordingPublisher();
    } else {
      throw new RuntimeException("Publisher not defined");
    }
  }

  @Bean @Lazy
  LocalRecordingPublisher localRecordingPublisher() {
    return new LocalRecordingPublisher(
        env.getRequiredProperty("localPublisher.rootDir"),
        env.getRequiredProperty("localPublisher.urlPrefix"),
        publishedRecordingPathEvaluator()
    );
  }

  @Bean @Lazy
  S3RecordingPublisher s3RecordingPublisher() {
    return new S3RecordingPublisher(
        s3FileRepository(),
        env.getRequiredProperty("s3Publisher.urlPrefix"),
        publishedRecordingPathEvaluator()
    );
  }

  @Bean @Lazy
  S3FileRepository s3FileRepository() {
    return new S3FileRepository(
        env.getRequiredProperty("s3Publisher.amazonServerAddress"),
        env.getRequiredProperty("s3Publisher.amazonKeyId"),
        env.getRequiredProperty("s3Publisher.amazonKey"),
        env.getRequiredProperty("s3Publisher.amazonBucket")
    );
  }

  @Bean @Lazy
  PublishedRecordingPathEvaluator publishedRecordingPathEvaluator() {
    return new DefaultPublishedRecordingPathEvaluator();
  }

  @Bean @Lazy
  GcmSender gcmSender() {
    return new GcmSender(
        env.getRequiredProperty("gcm.apiKey")
    );
  }

  @Bean
  CommandLineRunner init(ScheduledRecordingsProcessor scheduledRecordingsProcessor) {
    return args -> scheduledRecordingsProcessor.start();
  }

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
