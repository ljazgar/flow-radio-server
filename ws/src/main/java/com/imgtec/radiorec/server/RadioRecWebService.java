package com.imgtec.radiorec.server;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RadioRecWebService {

    @RequestMapping("/")
    String home() {
        return "Hello World!";
    }


}
