package com.imgtec.radiorec.server.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface RecordingRepository extends JpaRepository<Recording, Long> {

  Collection<Recording> findByUserIdOrderByStartTimeDesc(String userId);

}
