package com.imgtec.radiorec.server.shoutcast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class RadioRecorderImpl implements RadioRecorder {

  private static final Logger logger = LoggerFactory.getLogger(RadioRecorderImpl.class);

  private final ShoutcastStreamUrlResolver shoutcastStreamUrlResolver;
  private final ShoutcastRecorder shoutcastRecorder;

  @Autowired
  public RadioRecorderImpl(ShoutcastStreamUrlResolver shoutcastStreamUrlResolver, ShoutcastRecorder shoutcastRecorder) {
    this.shoutcastStreamUrlResolver = shoutcastStreamUrlResolver;
    this.shoutcastRecorder = shoutcastRecorder;
  }

  @Override
  public File recordRadio(String flowRadioUrl, int durationSecs) {
    String shoutcastUrl = shoutcastStreamUrlResolver.resolveStreamUrl(flowRadioUrl);
    logger.debug("Shoutcast URL: {}", shoutcastUrl);
    return shoutcastRecorder.recordStream(shoutcastUrl, durationSecs);
  }

}
