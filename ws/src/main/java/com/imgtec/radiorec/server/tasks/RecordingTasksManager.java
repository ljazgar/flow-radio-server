package com.imgtec.radiorec.server.tasks;


import com.google.common.util.concurrent.ListenableFuture;

public interface RecordingTasksManager {

  ListenableFuture<String> startRecording(String flowRadioUrl, int durationSecs, RecordingTaskMetaInfo metaInfo);

}
