package com.imgtec.radiorec.server.ws;

import com.google.common.base.Strings;
import com.imgtec.radiorec.server.model.Device;
import com.imgtec.radiorec.server.model.DeviceRepository;
import com.imgtec.radiorec.server.model.ScheduledRecording;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/{userId}/device")
public class DeviceRestController {

  private static final Logger logger = LoggerFactory.getLogger(DeviceRestController.class);

  private final DeviceRepository deviceRepository;

  @Autowired
  public DeviceRestController(DeviceRepository deviceRepository) {
    this.deviceRepository = deviceRepository;
  }

  @RequestMapping(method = RequestMethod.POST)
  ResponseEntity<?> add(@PathVariable String userId, @RequestBody Device deviceData) {
    validateNewDeviceData(deviceData);
    Device deviceToSave = adjustDeviceToSave(deviceData, userId);
    Device newDevice = deviceRepository.saveAndFlush(deviceToSave);

    logger.info("Device id={} for user: {} registered with gcmToken: {}",
        newDevice.getId(), newDevice.getUserId(), newDevice.getGcmToken());

    return new ResponseEntity<Object>(HttpStatus.CREATED);
  }

  private void validateNewDeviceData(Device deviceData) {
    if (Strings.isNullOrEmpty(deviceData.getGcmToken())) {
      throw new MandatoryPropertyException("gcmToken");
    }
  }

  private Device adjustDeviceToSave(Device deviceData, String userId) {
    deviceData.setId(null);
    deviceData.setUserId(userId);
    return deviceData;
  }
}


