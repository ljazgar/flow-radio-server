package com.imgtec.radiorec.server.shoutcast;

import java.io.File;

public interface RadioRecorder {

  class RadioRecordingException extends RuntimeException {
    public RadioRecordingException(String message) {
      super(message);
    }
    public RadioRecordingException(String message, Throwable cause) {
      super(message, cause);
    }
  }


  File recordRadio(String flowRadioUrl, int durationSecs);

}
