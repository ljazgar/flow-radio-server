package com.imgtec.radiorec.server.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface DeviceRepository extends JpaRepository<Device, Long> {

  Collection<Device> findByUserId(String userId);

}
