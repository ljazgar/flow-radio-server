package com.imgtec.radiorec.server.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface ScheduledRecordingRepository extends JpaRepository<ScheduledRecording, Long> {

  Collection<ScheduledRecording> findByUserIdOrderByStartTimeDesc(String userId);

  Collection<ScheduledRecording> findByUserIdAndStateInOrderByStartTimeDesc(
      String userId, Collection<ScheduledRecording.State> states);

  ScheduledRecording findTopByStateOrderByStartTimeAsc(ScheduledRecording.State state);

}
