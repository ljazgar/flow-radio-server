package com.imgtec.radiorec.server.s3;

import com.imgtec.radiorec.server.tasks.PublishedRecordingPathEvaluator;
import com.imgtec.radiorec.server.tasks.RecordingPublisher;
import com.imgtec.radiorec.server.tasks.RecordingTaskMetaInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class S3RecordingPublisher implements RecordingPublisher {

  private static final Logger logger = LoggerFactory.getLogger(S3RecordingPublisher.class);

  private final S3FileRepository s3FileRepository;
  private final String urlPrefix;
  private final PublishedRecordingPathEvaluator pathEvaluator;

  public S3RecordingPublisher(S3FileRepository s3FileRepository, String urlPrefix,
                              PublishedRecordingPathEvaluator pathEvaluator) {
    this.s3FileRepository = s3FileRepository;
    this.urlPrefix = urlPrefix;
    this.pathEvaluator = pathEvaluator;
    logger.debug("Creating S3RecordingPublisher. urlPrefix: {}", urlPrefix);
  }


  @Override
  public String publish(File tmpRecordingFile, RecordingTaskMetaInfo metaInfo) {

    String relativeFilePath = pathEvaluator.evaluateRelativePath(metaInfo);

    try {
      s3FileRepository.putFile(relativeFilePath, tmpRecordingFile);
    } catch (S3Exception e) {
      throw new PublishingException("Error while publishing recording on S3", e);
    }
    tmpRecordingFile.delete();

    return String.format("%s/%s", urlPrefix, relativeFilePath);
  }

}
