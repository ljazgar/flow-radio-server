package com.imgtec.radiorec.server.gcm;

import com.imgtec.radiorec.server.model.Recording;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class GcmSender {

  private static final Logger logger = LoggerFactory.getLogger(GcmSender.class);

  private final String apiKey;

  public GcmSender(String apiKey) {
    this.apiKey = apiKey;
  }

  public void sendNewRecordingNotification(String gcmToken, Recording newRecording) {
    try {
      // Prepare JSON containing the GCM message content. What to send and where to send.
      JSONObject jGcmData = new JSONObject();
      JSONObject jData = new JSONObject();
      jData.put("type", "newRecording");
      jData.put("userId", newRecording.getUserId());
      jData.put("recordingId", newRecording.getId());
      jData.put("recordingName", newRecording.getName());
      // Where to send GCM message.
      jGcmData.put("to", gcmToken);
      // What to send in GCM message.
      jGcmData.put("data", jData);

      // Create connection to send GCM Message request.
      URL url = new URL("https://android.googleapis.com/gcm/send");
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestProperty("Authorization", "key=" + apiKey);
      conn.setRequestProperty("Content-Type", "application/json");
      conn.setRequestMethod("POST");
      conn.setDoOutput(true);

      // Send GCM message content.
      OutputStream outputStream = conn.getOutputStream();
      String messageDataString = jGcmData.toString();
      logger.debug("Message sent: {}", messageDataString);
      outputStream.write(messageDataString.getBytes());

      // Read GCM response.
      InputStream inputStream = conn.getInputStream();
      String resp = IOUtils.toString(inputStream);
      logger.debug("Response: {}", resp);
    } catch (IOException e) {
      logger.error("Unable to send GCM message.", e);
    }
  }

}
