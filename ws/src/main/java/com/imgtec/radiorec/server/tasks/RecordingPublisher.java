package com.imgtec.radiorec.server.tasks;

import java.io.File;

public interface RecordingPublisher {

  class PublishingException extends RuntimeException {
    public PublishingException(String message) {
      super(message);
    }
    public PublishingException(String message, Throwable cause) {
      super(message, cause);
    }
  }

  /**
   *
   * @param tmpRecordingFile
   * @return url
   */
  String publish(File tmpRecordingFile, RecordingTaskMetaInfo metaInfo);

}
