package com.imgtec.radiorec.server.ws;

import com.google.common.base.Strings;
import com.imgtec.radiorec.server.model.ScheduledRecording;
import com.imgtec.radiorec.server.model.ScheduledRecordingRepository;
import com.imgtec.radiorec.server.processor.ScheduledRecordingsProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

@RestController
@RequestMapping("/user/{userId}/scheduled")
public class ScheduledRecordingRestController {

  private static final Logger logger = LoggerFactory.getLogger(ScheduledRecordingRestController.class);

  private final ScheduledRecordingRepository scheduledRecordingRepository;
  private final ScheduledRecordingsProcessor scheduledRecordingsProcessor;

  @Autowired
  public ScheduledRecordingRestController(ScheduledRecordingRepository scheduledRecordingRepository,
                                          ScheduledRecordingsProcessor scheduledRecordingsProcessor) {
    this.scheduledRecordingRepository = scheduledRecordingRepository;
    this.scheduledRecordingsProcessor = scheduledRecordingsProcessor;
  }

  @RequestMapping(value = "/{scheduledRecordingId}", method = RequestMethod.GET)
  ScheduledRecording readScheduledRecording(@PathVariable String userId, @PathVariable Long scheduledRecordingId) {
    ScheduledRecording scheduledRecording = this.scheduledRecordingRepository.findOne(scheduledRecordingId);
    if (scheduledRecording != null && scheduledRecording.getUserId().equals(userId)) {
      return scheduledRecording;
    } else {
      throw new ScheduledRecordingNotFoundException(scheduledRecordingId);
    }
  }

  public static final String FILTER_ALL = "all";
  public static final String FILTER_ARCHIVAL = "archival";
  public static final String FILTER_CURRENT = "current";

  @RequestMapping(method = RequestMethod.GET)
  Collection<ScheduledRecording> readScheduledRecordings(
      @PathVariable String userId,
      @RequestParam(value = "filter", required = false, defaultValue = "all") String filter) {

    if (FILTER_CURRENT.equalsIgnoreCase(filter)) {
      return scheduledRecordingRepository.findByUserIdAndStateInOrderByStartTimeDesc(
          userId, Arrays.asList(ScheduledRecording.State.WAITING, ScheduledRecording.State.RECORDING));

    } else if (FILTER_ARCHIVAL.equalsIgnoreCase(filter)) {
      return scheduledRecordingRepository.findByUserIdAndStateInOrderByStartTimeDesc(
          userId, Arrays.asList(ScheduledRecording.State.DONE, ScheduledRecording.State.ERROR,
              ScheduledRecording.State.OMITTED));

    } else if (FILTER_ALL.equalsIgnoreCase(filter)) {
      return scheduledRecordingRepository.findByUserIdOrderByStartTimeDesc(userId);

    } else {
      return Collections.emptyList();
    }
  }

  @RequestMapping(method = RequestMethod.POST)
  ResponseEntity<?> add(@PathVariable String userId, @RequestBody ScheduledRecording scheduledRecordingData) {
    validateNewScheduledRecordingData(scheduledRecordingData);
    ScheduledRecording scheduledRecordingToSave = adjustScheduledRecordingToSave(scheduledRecordingData, userId);
    ScheduledRecording result = scheduledRecordingRepository.saveAndFlush(scheduledRecordingToSave);

    scheduledRecordingsProcessor.scheduleChanged();

    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setLocation(ServletUriComponentsBuilder
        .fromCurrentRequest()
        .path("/{id}")
        .buildAndExpand(result.getId())
        .toUri());
    return new ResponseEntity<Object>(null, httpHeaders, HttpStatus.CREATED);
  }

  private void validateNewScheduledRecordingData(ScheduledRecording scheduledRecordingData) {
    if (Strings.isNullOrEmpty(scheduledRecordingData.getName())) {
      throw new MandatoryPropertyException("name");
    } else if (Strings.isNullOrEmpty(scheduledRecordingData.getFlowRadioId())) {
      throw new MandatoryPropertyException("flowRadioId");
    } else if (Strings.isNullOrEmpty(scheduledRecordingData.getRadioStreamUrl())) {
      throw new MandatoryPropertyException("radioStreamUrl");
    } else if (scheduledRecordingData.getEndTime() == null) {
      throw new MandatoryPropertyException("endTime");
    } else if (scheduledRecordingData.getStartTime() == null) {
      throw new MandatoryPropertyException("startTime");
    } else if (!scheduledRecordingData.getStartTime().before(scheduledRecordingData.getEndTime())) {
      throw new InvalidPropertyValueException("endTime cannot be before startTime");
    } else if (scheduledRecordingData.getStartTime().before(new Date())) {
      throw new InvalidPropertyValueException("startTime cannot be date in the past");
    }
  }

  private ScheduledRecording adjustScheduledRecordingToSave(ScheduledRecording scheduledRecordingData, String userId) {
    scheduledRecordingData.setState(ScheduledRecording.State.WAITING);
    scheduledRecordingData.setId(null);
    scheduledRecordingData.setUserId(userId);
    return scheduledRecordingData;
  }

  @RequestMapping(value = "/{scheduledRecordingId}", method = RequestMethod.DELETE)
  void delete(@PathVariable String userId, @PathVariable Long scheduledRecordingId) {
    ScheduledRecording scheduledRecording = this.scheduledRecordingRepository.findOne(scheduledRecordingId);
    if (scheduledRecording == null || !scheduledRecording.getUserId().equals(userId)) {
      throw new ScheduledRecordingNotFoundException(scheduledRecordingId);
    }

    ScheduledRecording.State state = scheduledRecording.getState();
    if (state == ScheduledRecording.State.DONE
        || state == ScheduledRecording.State.ERROR
        || state == ScheduledRecording.State.OMITTED) {
      throw new IllegalStateException(String.format("Scheduled recording id=%d cannot be deleted, because it is in one of archival states - %s",
          scheduledRecordingId, state));
    }

    scheduledRecordingRepository.delete(scheduledRecording);
    logger.debug("Scheduled recording id={} with state {} deleted", scheduledRecordingId, state);
    scheduledRecordingsProcessor.scheduleChanged();
  }

}


@ResponseStatus(HttpStatus.NOT_FOUND)
class ScheduledRecordingNotFoundException extends RuntimeException {
  public ScheduledRecordingNotFoundException(Long scheduledRecordingId) {
    super("Could not find scheduled recording with id '" + scheduledRecordingId + "'");
  }
}

@ResponseStatus(HttpStatus.BAD_REQUEST)
class MandatoryPropertyException extends RuntimeException {
  public MandatoryPropertyException(String property) {
    super("Property '" + property + "' is mandatory");
  }
}

@ResponseStatus(HttpStatus.BAD_REQUEST)
class InvalidPropertyValueException extends RuntimeException {
  public InvalidPropertyValueException(String message) {
    super(message);
  }
  public InvalidPropertyValueException(String property, String value) {
    super("Property '" + property + "' has invalid value (" + value +")");
  }
}

@ResponseStatus(HttpStatus.BAD_REQUEST)
class IllegalStateException extends RuntimeException {
  public IllegalStateException(String message) {
    super(message);
  }
}
