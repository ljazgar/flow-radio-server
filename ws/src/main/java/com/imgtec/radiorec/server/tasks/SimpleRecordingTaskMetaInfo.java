package com.imgtec.radiorec.server.tasks;

public class SimpleRecordingTaskMetaInfo implements RecordingTaskMetaInfo {

  private final String userId;
  private final String recordingName;
  private final String flowRadioId;

  public SimpleRecordingTaskMetaInfo(String userId, String flowRadioId, String recordingName) {
    this.userId = userId;
    this.recordingName = recordingName;
    this.flowRadioId = flowRadioId;
  }

  @Override
  public String getUserId() {
    return userId;
  }

  @Override
  public String getFlowRadioId() {
    return flowRadioId;
  }


  @Override
  public String getRecordingName() {
    return recordingName;
  }

}
