package com.imgtec.radiorec.server.tasks;

public interface RecordingTaskMetaInfo {

  String getUserId();
  String getFlowRadioId();
  String getRecordingName();

}
