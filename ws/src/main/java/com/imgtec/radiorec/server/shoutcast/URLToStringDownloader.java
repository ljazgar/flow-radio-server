package com.imgtec.radiorec.server.shoutcast;

import com.google.common.io.CharStreams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;

@Component
public class URLToStringDownloader {

  private static final Logger logger = LoggerFactory.getLogger(URLToStringDownloader.class);

  private final ClientHttpRequestFactory clientHttpRequestFactory;

  public static class DownloadException extends RuntimeException {
    public DownloadException(String message) {
      super(message);
    }
    public DownloadException(String message, Throwable cause) {
      super(message, cause);
    }
  }

  @Autowired
  public URLToStringDownloader(ClientHttpRequestFactory clientHttpRequestFactory) {
    this.clientHttpRequestFactory = clientHttpRequestFactory;
  }

  String download(String url) {
    ClientHttpRequest request = null;
    try {
      request = clientHttpRequestFactory.createRequest(URI.create(url), HttpMethod.GET);
    } catch (IOException e) {
      throw new DownloadException(String.format("Creating request to URL: '%s' failed", url), e);
    }

    logger.debug("Sending request for url: {}", url);
    try (
        ClientHttpResponse response = request.execute()
    ) {
      if (response.getStatusCode() != HttpStatus.OK) {
        throw new DownloadException(
            String.format("Http response from URL: '%s' has invalid status code: %s", url, response.getStatusText()));
      }

      InputStream bodyInputStream = response.getBody();
      Reader bodyReader = new InputStreamReader(bodyInputStream);

      return CharStreams.toString(bodyReader);
    } catch (IOException e) {
      throw new DownloadException(
          String.format("Exception occurred while fetching body of Http response from URL: '%s'", url), e);
    }
  }

}
