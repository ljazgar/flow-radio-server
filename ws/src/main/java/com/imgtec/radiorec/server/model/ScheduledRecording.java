package com.imgtec.radiorec.server.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ScheduledRecording {

  public enum State {
    WAITING, RECORDING, DONE, ERROR, OMITTED
  }

  public enum RepeatOption {
    DAILY, WEEKLY, MONTHLY
  }

  @Id
  @GeneratedValue
  private Long id;

  @Column(nullable = false)
  private String userId;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private State state;

  /**
   * Flow.LiveRadioItem.ContentItemId
   */
  @Column(nullable = false)
  private String flowRadioId;

  @Column(nullable = false)
  private String radioStreamUrl;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private Date startTime;

  @Column(nullable = false)
  private Date endTime;

  @Column(nullable = true)
  @Enumerated(EnumType.STRING)
  private RepeatOption repeatOption;

  public ScheduledRecording() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getFlowRadioId() {
    return flowRadioId;
  }

  public void setFlowRadioId(String flowRadioId) {
    this.flowRadioId = flowRadioId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public String getRadioStreamUrl() {
    return radioStreamUrl;
  }

  public void setRadioStreamUrl(String radioStreamUrl) {
    this.radioStreamUrl = radioStreamUrl;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public RepeatOption getRepeatOption() {
    return repeatOption;
  }

  public void setRepeatOption(RepeatOption repeatOption) {
    this.repeatOption = repeatOption;
  }
}
