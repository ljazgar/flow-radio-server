package com.imgtec.radiorec.server.shoutcast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;

import java.io.File;

@Configuration
@ComponentScan
@Component
public class ShoutcastTest {

  private static final Logger logger = LoggerFactory.getLogger(ShoutcastTest.class);

  @Bean
  ClientHttpRequestFactory clientHttpRequestFactory() {
    return new SimpleClientHttpRequestFactory();
  }

  @Autowired
  ShoutcastStreamUrlResolver shoutcastStreamUrlResolver;

  @Autowired
  ShoutcastRecorder shoutcastRecorder;

  private void testM3UDownloader() {
    testFlowStreamUrl( "http://media.thelounge.com/radio/27.m3u" ); // BBC Radio 4
    testFlowStreamUrl( "http://media.thelounge.com/radio/6567.m3u" ); // France Inter
    testFlowStreamUrl( "http://media.thelounge.com/radio/13053.mp3.m3u" ); // RMF FM
    testFlowStreamUrl( "http://media.thelounge.com/radio/43972.m3u" );
  }

  private void testFlowStreamUrl(String flowStreamUrl) {
    logger.debug("Resolving shoutcast url for flowStreamUrl: {}", flowStreamUrl);
    String shoutcastUrl = shoutcastStreamUrlResolver.resolveStreamUrl(flowStreamUrl);
    if (shoutcastUrl != null) {
      logger.debug("Shoutcast URL: {}", shoutcastUrl);
    } else {
      logger.error("Cannot resolve shoutcast url for: {}", flowStreamUrl);
    }
  }

  private void testShoutcastRecording() {
    File recordFile = shoutcastRecorder.recordStream(
        "http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio4fm_mf_p?s=1445945908&e=1445960308&h=7da442435a4d564f98164700299132ef",
        30);

    File targetFile = new File("c:/tmp/miquido/radiorec/recording.mp3");
    targetFile.delete();
    recordFile.renameTo(targetFile);
  }

  public static void main(String[] args) {
    ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ShoutcastTest.class);
    ShoutcastTest shoutcastTest = applicationContext.getBean(ShoutcastTest.class);
//    shoutcastTest.testM3UDownloader();
    shoutcastTest.testShoutcastRecording();
  }


}
