package com.imgtec.radiorec.server.shoutcast;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.imgtec.radiorec.server.tasks.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.concurrent.ExecutionException;

@Configuration
@ComponentScan(basePackages = {"com.imgtec.radiorec.server.tasks", "com.imgtec.radiorec.server.shoutcast"})
@Component
public class RecordingTaskTest {

  private static final Logger logger = LoggerFactory.getLogger(RecordingTaskTest.class);

  @Bean
  ClientHttpRequestFactory clientHttpRequestFactory() {
    return new SimpleClientHttpRequestFactory();
  }

  @Bean
  RecordingPublisher recordingPublisher() {
    return new LocalRecordingPublisher("c:/tmp/miquido/radiorec", "http://www.imgtec.com/recordings",
        publishedRecordingPathEvaluator());
  }

  @Bean @Lazy
  PublishedRecordingPathEvaluator publishedRecordingPathEvaluator() {
    return new DefaultPublishedRecordingPathEvaluator();
  }

  @Autowired
  RecordingTasksManager recordingTasksManager;

  public static void main(String[] args) {
    ApplicationContext applicationContext = new AnnotationConfigApplicationContext(RecordingTaskTest.class);
    RecordingTaskTest recordingTaskTest = applicationContext.getBean(RecordingTaskTest.class);
    recordingTaskTest.testRecordingTasks();
  }


  private void testRecordingTasks() {

    // BBC Radio 4
    ListenableFuture<String> bbcRecordingFuture =
        recordingTasksManager.startRecording("http://media.thelounge.com/radio/27.m3u", 20,
            new SimpleRecordingTaskMetaInfo("13", "123", "BBC news") );
    Futures.addCallback(bbcRecordingFuture, new RecordingFutureCallback("BBC"));

    // RMF FM
    ListenableFuture<String> rmfRecordingFuture =
        recordingTasksManager.startRecording("http://media.thelounge.com/radio/13053.mp3.m3u", 40,
            new SimpleRecordingTaskMetaInfo("13", "124", "BBC news") );
    Futures.addCallback(rmfRecordingFuture, new RecordingFutureCallback("RMF"));


    try {
      bbcRecordingFuture.get();
      rmfRecordingFuture.get();
    } catch (InterruptedException | ExecutionException e) {
      logger.error("Error:", e);
    }
    logger.debug("Test finished");
  }


  private static class RecordingFutureCallback implements FutureCallback<String> {
    private final String radioName;

    private RecordingFutureCallback(String radioName) {
      this.radioName = radioName;
    }

    @Override
    public void onSuccess(String url) {
      logger.error(String.format("Radio %s recorded and is accessible from url: %s", radioName, url));
    }

    @Override
    public void onFailure(Throwable t) {
      logger.error(String.format("Error while recording radio %s", radioName), t);
    }

  }

}
